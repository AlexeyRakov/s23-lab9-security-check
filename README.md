# Lab9 -- Security check

**Website:** https://dtf.ru/
## Forgot password

    Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-cheat-sheet

Steps

1. exist an account at https://dtf.ru/

- go to https://www.dtf.ru 
- click login, click enter with existed account, select email enter
- enter email 
- check the response body and time via Devtools

2. do not exist an account at https://dtf.ru/

- checkout to https://dtf.ru enter unexisted email or login
- check the response body and time via Devtools, check the mail for
  the letter
- check there is captcha on resetting

| Test step                                                         | Result               |
| ----------------------------------------                          | -------------------- |
| go to https://dtf.ru/                                             | Ok                   |
| click login, click enter with existed account, select email enter | Ok                   |
| enter valid email                                                 | Ok                   |
| click "Reset Password"                                   | Ok                   |
| check the response of Post request                                | response - "link for reseting sent to email", message shown to user, time 390ms|
| checkout to https://dtf.ru/                                       | Ok                   |
| enter non-existing email                                    | Ok                   |
| click "Reset password"                                   | Ok                   |
| check the response of Post request                                | response - "no such user", message not shown to user, time - 289ms |
| check there is captcha on resetting | there is no captcha on resetting, possible send a lot of requests. but site hosted on cloudflare, so possibly in case of ddos attack - captcha from host will shown|

### Result

If account exist - email with link sent to given email, if there is no account with email - happen nothing for user. There is no in-site captcha for requesting of resetting. There is no valuable difference in time of resetting for exist and non-exist user

## Input validation

    Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html

Steps

1. go to https://dtf.ru/, click login, click signup
2. make sure there is min and max limit of characters for password and login and check on password strongness
3. make sure that empty email or invalid email cant be submitted

| Test step                                 | Result                                          |
| ----------------------------------------- | ----------------------------------------------- |
| go to https://dtf.ru/, click login, click signup         | Ok                                              |
| make sure there is min limit for password |    there is no minimum limit (send request with 2 letters)                           |
| make sure there is max limit for password | there is no maximum limit (send request with more than 150 symbols) |
|make sure there is password strongness check | there is no strongness check|
| make sure that impossible send empty fields |       Ok                  |
| make sure there is max limit for login    |  Ok                          |
| make sure in rejecting invalid email                  |             Ok                |

### Result
There is no check on minimum password and password strongness, all other steps are passed

## User-ids test

    Cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html#ensure-lookup-ids-are-not-accessible-even-when-guessed-or-cannot-be-tampered-with

Steps

1. check if there is some id on users account
2. check on authorized account get access to settings of account
3. check access to settings with id of some another user on authorized account
4. check access to settings with id of some user on unauthorized account

| Test step                                           | Result               |
| --------------------------------------------------- | -------------------- |
| check if there is some id on users account | there is number id + name created on signup |
| check on logined account get access to settings of account | Ok |
| check access to settings with id of some another user | 403 error with a message that there is no access right to resource|
| check access to settings with id of some user on unauthorized account | 403 error with a message that there is no access right to resource 


### Result
All tests are passed, there is no access for guest users to accounts. Also one authorized user can not get access to some sensitive information of another user 
